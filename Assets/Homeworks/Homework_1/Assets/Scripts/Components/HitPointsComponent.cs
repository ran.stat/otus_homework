using System;
using UnityEngine;

namespace Homeworks.Homework_1
{
    public sealed class HitPointsComponent : MonoBehaviour
    {
        public event Action<GameObject> Death;

        [SerializeField] private int hitPoints;

        public bool IsHitPointsExists()
        {
            return hitPoints > 0;
        }

        public void TakeDamage(int damage)
        {
            hitPoints -= damage;
            if (hitPoints <= 0)
            {
                Death?.Invoke(gameObject);
            }
        }
    }
}