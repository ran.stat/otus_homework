using UnityEngine;

namespace Homeworks.Homework_1
{
    public sealed class TeamComponent : MonoBehaviour
    {
        public bool IsPlayer
        {
            get { return this.isPlayer; }
        }
        
        [SerializeField]
        private bool isPlayer;
    }
}