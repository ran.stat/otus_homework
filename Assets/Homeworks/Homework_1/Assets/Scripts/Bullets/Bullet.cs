using System;
using UnityEngine;

namespace Homeworks.Homework_1
{
    public sealed class Bullet : MonoBehaviour
    {
        public event Action<Bullet, Collision2D> OnCollisionEntered;

        [HideInInspector] public bool IsPlayer;
        [HideInInspector] public int Damage;

        public Rigidbody2D _rigidbody;
        private SpriteRenderer _spriteRenderer;

        public void Init()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            OnCollisionEntered?.Invoke(this, collision);
        }

        public void SetVelocity(Vector2 velocity)
        {
            _rigidbody.velocity = velocity;
        }

        public void SetPhysicsLayer(int physicsLayer)
        {
            gameObject.layer = physicsLayer;
        }

        public void SetPosition(Vector3 position)
        {
            transform.position = position;
        }

        public void SetColor(Color color)
        {
            _spriteRenderer.color = color;
        }

        public Vector2 GetVelocity()
        {
            return _rigidbody.velocity;
        }
    }
}