using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Homeworks.Homework_1
{
    public sealed class BulletSystem : MonoBehaviour, 
        IGameFixedUpdateListener,
        IGameInitListener,
        IGamePauseListener,
        IGameResumeListener
    {
        [SerializeField] private int initialCount = 50;
        [SerializeField] private Transform container;
        [SerializeField] private Bullet prefab;
        [SerializeField] private Transform worldTransform;
        
        [Inject] private LevelBounds _levelBounds;

        private readonly Queue<Bullet> m_bulletPool = new Queue<Bullet>();
        private readonly List<Bullet> m_activeBullets = new List<Bullet>();
        private readonly List<Bullet> m_cache = new List<Bullet>();
        
        private List<Vector2> _currentVelocityActiveBullet = new List<Vector2>();

        public void OnInit()
        {
            for (var i = 0; i < initialCount; i++)
            {
                var bullet = Instantiate(prefab, container);
                bullet.Init();
                m_bulletPool.Enqueue(bullet);
            }

            for (int i = 0; i < initialCount; i++)
            {
                _currentVelocityActiveBullet.Add(Vector2.zero);
            }
        }
        
        public void OnFixedUpdate(float deltaTime)
        {
            m_cache.Clear();
            m_cache.AddRange(m_activeBullets);

            for (int i = 0, count = m_cache.Count; i < count; i++)
            {
                var bullet = m_cache[i];
                if (!_levelBounds.InBounds(bullet.transform.position))
                {
                    RemoveBullet(bullet);
                }
            }
        }
        
        public void OnPauseGame()
        {
            for (int i = 0; i < m_activeBullets.Count; i++)
            {
                _currentVelocityActiveBullet[i] = m_activeBullets[i].GetVelocity();
                m_activeBullets[i].SetVelocity(Vector2.zero);
            }
        }

        public void OnResumeGame()
        {
            for (int i = 0; i < m_activeBullets.Count; i++)
            {
                m_activeBullets[i].SetVelocity(_currentVelocityActiveBullet[i]);
            }
        }

        public void FlyBulletByArgs(Args args)
        {
            if (m_bulletPool.TryDequeue(out var bullet))
            {
                bullet.transform.SetParent(worldTransform);
            }
            else
            {
                bullet = Instantiate(prefab, worldTransform);
            }

            bullet.SetPosition(args.Position);
            bullet.SetColor(args.Color);
            bullet.SetPhysicsLayer(args.PhysicsLayer);
            bullet.Damage = args.Damage;
            bullet.IsPlayer = args.IsPlayer;
            bullet.SetVelocity(args.Velocity);
            
            m_activeBullets.Add(bullet);
            bullet.OnCollisionEntered += OnBulletCollision;

        }

        private void OnBulletCollision(Bullet bullet, Collision2D collision)
        {
            BulletUtils.DealDamage(bullet, collision.gameObject);
            RemoveBullet(bullet);
        }

        private void RemoveBullet(Bullet bullet)
        {
            if (m_activeBullets.Remove(bullet))
            {
                bullet.OnCollisionEntered -= OnBulletCollision;
                bullet.transform.SetParent(container);
                m_bulletPool.Enqueue(bullet);
            }
        }
        
        public struct Args
        {
            public Vector2 Position;
            public Vector2 Velocity;
            public Color Color;
            public int PhysicsLayer;
            public int Damage;
            public bool IsPlayer;
        }

        
    }
}