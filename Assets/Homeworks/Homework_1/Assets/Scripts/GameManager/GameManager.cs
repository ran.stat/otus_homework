using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Homeworks.Homework_1
{
    public enum GameState
    {
        OFF = 0,
        PLAYING = 1,
        PAUSED = 2,
        FINISHED = 3
    }

    public sealed class GameManager : MonoBehaviour
    {

        public static GameManager Instance;

        private readonly List<IGameListener> _listeners = new List<IGameListener>();
        private readonly List<IGameStartListener> _startListeners = new List<IGameStartListener>();
        private readonly List<IGameFinishListener> _finishListeners = new List<IGameFinishListener>();
        private readonly List<IGameResumeListener> _resumeListeners = new List<IGameResumeListener>();
        private readonly List<IGamePauseListener> _pauseListeners = new List<IGamePauseListener>();
        private readonly List<IGameUpdateListener> _updateListeners = new List<IGameUpdateListener>();
        private readonly List<IGameLateUpdateListener> _lateUpdateListeners = new List<IGameLateUpdateListener>();
        private readonly List<IGameFixedUpdateListener> _fixedUpdateListeners = new List<IGameFixedUpdateListener>();

        private GameState _state = GameState.OFF;

        public GameState State => _state;

        void Awake()
        {
            if (Instance == null) Instance = this;
        }

        private void Start()
        {
            Debug.Log("Init");
            InitGame();
            StartGame();
        }

        private void Update()
        {
            if (_state != GameState.PLAYING) return;

            foreach (var updateListener in _updateListeners)
            {
                updateListener.OnUpdate(Time.deltaTime);
            }
        }

        private void LateUpdate()
        {
            if (_state != GameState.PLAYING) return;

            foreach (var updateListener in _lateUpdateListeners)
            {
                updateListener.OnLateUpdate(Time.deltaTime);
            }
        }
        
        private void FixedUpdate()
        {
            if (_state != GameState.PLAYING) return;

            foreach (var updateListener in _fixedUpdateListeners)
            {
                updateListener.OnFixedUpdate(Time.deltaTime);
            }
        }

        public void AddListener(IGameListener listener)
        {
            if (listener == null) return;

            _listeners.Add(listener);

            if (listener is IGameFinishListener finishListener)
            {
                _finishListeners.Add(finishListener);
            }

            if (listener is IGamePauseListener pauseListener)
            {
                _pauseListeners.Add(pauseListener);
            }

            if (listener is IGameResumeListener resumeListener)
            {
                _resumeListeners.Add(resumeListener);
            }

            if (listener is IGameStartListener startListener)
            {
                _startListeners.Add(startListener);
            }

            if (listener is IGameUpdateListener updateListener)
            {
                _updateListeners.Add(updateListener);
            }

            if (listener is IGameLateUpdateListener lateUpdateListener)
            {
                _lateUpdateListeners.Add(lateUpdateListener);
            }
            
            if (listener is IGameFixedUpdateListener fixedUpdateListener)
            {
                _fixedUpdateListeners.Add(fixedUpdateListener);
            }

        }

        [Button]
        public void InitGame()
        {
            foreach (var listener in _listeners)
            {
                if (listener is IGameInitListener initListener)
                {
                    initListener.OnInit();
                }
            }
        }

        [Button]
        public void StartGame()
        {
            foreach (var startListener in _startListeners)
            {
                startListener.OnStartGame();
            }
            ChangeState(GameState.PLAYING);
        }

        [Button]
        public void FinishGame()
        {
            foreach (var finishListener in _finishListeners)
            {
                finishListener.OnFinishGame();
            }

            ChangeState(GameState.FINISHED);
            Debug.Log("Game over!");
            Time.timeScale = 0;
        }

        [Button]
        public void PauseGame()
        {
            foreach (var pauseListener in _pauseListeners)
            {
                pauseListener.OnPauseGame();
            }
            ChangeState(GameState.PAUSED);
        }

        [Button]
        public void ResumeGame()
        {
            foreach (var resumeListener in _resumeListeners)
            {
                resumeListener.OnResumeGame();
            }
            ChangeState(GameState.PLAYING);
        }

        private void ChangeState(GameState state)
        {
            _state = state;
            Debug.Log(_state);
        }
    }
}