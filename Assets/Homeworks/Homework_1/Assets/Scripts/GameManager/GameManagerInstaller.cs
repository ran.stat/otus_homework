﻿using UnityEngine;

namespace Homeworks.Homework_1
{
    public class GameManagerInstaller : MonoBehaviour
    {
        private void Awake()
        {
            var gameManager = GetComponent<GameManager>();
            var listeners = GetComponentsInChildren<IGameListener>();

            foreach (var listener in listeners)
            {
                gameManager.AddListener(listener);
            }
        }
    }
}