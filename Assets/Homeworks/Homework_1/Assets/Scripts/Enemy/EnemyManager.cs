using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Homeworks.Homework_1
{
    public sealed class EnemyManager : MonoBehaviour,
        IGameFixedUpdateListener,
        IGameUpdateListener
    {
        [Inject] private EnemyPool _enemyPool;
        [Inject] private BulletSystem _bulletSystem;

        [SerializeField] private float timeSpan = 1f;
        [SerializeField] private BulletConfig bulletConfig;
        
        private readonly List<EnemyController> m_activeEnemies = new List<EnemyController>();
        private float _lastTime;

        public void OnUpdate(float deltaTime)
        {
            if (Time.time - _lastTime > timeSpan)
            {
                _lastTime = Time.time;
                var enemy = _enemyPool.SpawnEnemy();
                if (enemy != null)
                {
                    var controller = enemy.GetComponent<EnemyController>();
                    m_activeEnemies.Add(controller);
                    controller.Init();
                    controller.HitPointsComponent.Death += OnDestroyed;
                    controller.AttackAgent.OnFire += OnFire;
                }
            }
        }

        public void OnFixedUpdate(float deltaTime)
        {
            foreach (var enemyController in m_activeEnemies)
            {
                enemyController.AttackAgent.OnFixedUpdate(deltaTime);
                enemyController.MoveAgent.OnFixedUpdate(deltaTime);
            }
        }
        

        private void OnDestroyed(GameObject enemy)
        {
            var controller = enemy.GetComponent<EnemyController>();
            if (m_activeEnemies.Remove(controller))
            {
                controller.HitPointsComponent.Death -= OnDestroyed;
                controller.AttackAgent.OnFire -= OnFire;

                _enemyPool.UnspawnEnemy(enemy);
            }
        }

        private void OnFire(Vector2 position, Vector2 direction)
        {
            _bulletSystem.FlyBulletByArgs(new BulletSystem.Args
            {
                IsPlayer = false,
                PhysicsLayer = (int) bulletConfig.PhysicsLayer,
                Color = bulletConfig.Color,
                Damage = bulletConfig.Damage,
                Position = position,
                Velocity = direction * bulletConfig.Speed
            });
        }

        
    }
}