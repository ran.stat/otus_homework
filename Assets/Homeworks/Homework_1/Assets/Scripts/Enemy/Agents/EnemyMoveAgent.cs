using UnityEngine;

namespace Homeworks.Homework_1
{
    public sealed class EnemyMoveAgent : MonoBehaviour
    {
        public bool IsReached;

        [SerializeField] private MoveComponent moveComponent;

        private Vector2 destination;

        public void SetDestination(Vector2 endPoint)
        {
            destination = endPoint;
            IsReached = false;
        }

        public void OnFixedUpdate(float deltaTime)
        {
            if (IsReached)
            {
                return;
            }
            
            var vector = destination - (Vector2) transform.position;
            if (vector.magnitude <= 0.25f)
            {
                IsReached = true;
                return;
            }

            var direction = vector.normalized * Time.fixedDeltaTime;
            moveComponent.MoveByRigidbodyVelocity(direction);
        }
    }
}