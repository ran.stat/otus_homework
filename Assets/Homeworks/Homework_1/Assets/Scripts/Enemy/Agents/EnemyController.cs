﻿using UnityEngine;

namespace Homeworks.Homework_1
{
    public class EnemyController : MonoBehaviour
    {
        [HideInInspector] public EnemyAttackAgent AttackAgent;
        [HideInInspector] public EnemyMoveAgent MoveAgent;
        [HideInInspector] public HitPointsComponent HitPointsComponent;

        public void Init()
        {
            AttackAgent = GetComponent<EnemyAttackAgent>();
            MoveAgent = GetComponent<EnemyMoveAgent>();
            HitPointsComponent = GetComponent<HitPointsComponent>();
        }
    }
}