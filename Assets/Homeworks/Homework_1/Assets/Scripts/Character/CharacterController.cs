using UnityEngine;
using Zenject;

namespace Homeworks.Homework_1
{
    public sealed class CharacterController : MonoBehaviour, 
        IGameFixedUpdateListener,
        IGameInitListener,
        IGameFinishListener,
        IGamePauseListener,
        IGameUpdateListener,
        IGameResumeListener
    {
        public GameObject Character;
        
        [Inject] private GameManager _gameManager;
        [Inject] private KeyboardInput _keyboardInput;
        [Inject] private LevelBounds _levelBounds;

        [SerializeField] private BulletSystem bulletSystem;
        [SerializeField] private BulletConfig bulletConfig;
        [SerializeField] private float speed = 2f;
        
        private MoveComponent _moveComponent;
        private bool _fireRequired = false;

        private void OnCharacterDeath(GameObject _) => _gameManager.FinishGame();

        public void OnInit()
        {
            _moveComponent = Character.GetComponent<MoveComponent>();
            Character.GetComponent<HitPointsComponent>().Death += OnCharacterDeath;
            _keyboardInput.OnMoveLeft += MoveLeft;
            _keyboardInput.OnMoveRight += MoveRight;
            _keyboardInput.OnFire += TryFire;
        }

        void IGameUpdateListener.OnUpdate(float deltaTime)
        {
            _keyboardInput.OnUpdate(deltaTime);
        }

        void IGameFixedUpdateListener.OnFixedUpdate(float deltaTime)
        {
            if (_fireRequired)
            {
                OnFire();
                _fireRequired = false;
            }
        }
        
        void IGameFinishListener.OnFinishGame()
        {
            _keyboardInput.OnMoveLeft -= MoveLeft;
            _keyboardInput.OnMoveRight -= MoveRight;
            _keyboardInput.OnFire -= TryFire;
            if (Character != null)
                Character.GetComponent<HitPointsComponent>().Death -= OnCharacterDeath;
        }

        void IGamePauseListener.OnPauseGame()
        {
            _keyboardInput.OnMoveLeft -= MoveLeft;
            _keyboardInput.OnMoveRight -= MoveRight;
            _keyboardInput.OnFire -= TryFire;
        }
        
        public void OnResumeGame()
        {
            _keyboardInput.OnMoveLeft += MoveLeft;
            _keyboardInput.OnMoveRight += MoveRight;
            _keyboardInput.OnFire += TryFire;
        }

        private void TryFire()
        {
            _fireRequired = true;
        }

        private void OnFire()
        {
            var weapon = Character.GetComponent<WeaponComponent>();
            bulletSystem.FlyBulletByArgs(new BulletSystem.Args
            {
                IsPlayer = true,
                PhysicsLayer = (int) bulletConfig.PhysicsLayer,
                Color = bulletConfig.Color,
                Damage = bulletConfig.Damage,
                Position = weapon.Position,
                Velocity = weapon.Rotation * Vector3.up * bulletConfig.Speed
            });
        }
        
        private void MoveLeft()
        {
            if (!_levelBounds.TryLeft(Character.transform.position))
            {
                return;
            }
            _moveComponent.MoveByRigidbodyVelocity(Vector2.left * (Time.fixedDeltaTime * speed));
        }

        private void MoveRight()
        {
            if (!_levelBounds.TryRight(Character.transform.position))
            {
                return;
            }
            _moveComponent.MoveByRigidbodyVelocity(Vector2.right * (Time.fixedDeltaTime * speed));
        }


        
    }
}