﻿using Zenject;

namespace Homeworks.Homework_1
{
    public class SceneInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<GameManager>().FromComponentInHierarchy().AsSingle();
            Container.Bind<CharacterController>().FromComponentInHierarchy().AsSingle();
            Container.Bind<BulletSystem>().FromComponentInHierarchy().AsSingle();
            Container.Bind<EnemyPool>().FromComponentInHierarchy().AsSingle();
            Container.Bind<LevelBounds>().FromComponentInHierarchy().AsSingle();
            Container.Bind<EnemyPositions>().FromComponentInHierarchy().AsSingle();
            Container.Bind<KeyboardInput>().FromComponentInHierarchy().AsSingle();
        }
    }
}