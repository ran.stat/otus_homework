namespace Homeworks.Homework_1
{
    public enum PhysicsLayer
    {
        CHARACTER = 10,
        ENEMY = 11,
        ENEMY_BULLET = 13,
        PLAYER_BULLET = 14
    }
}