using System;
using UnityEngine;

namespace Homeworks.Homework_1
{
    public sealed class KeyboardInput : MonoBehaviour
    {
        public Action OnMoveLeft;
        public Action OnMoveRight;
        public Action OnFire;

        public void OnUpdate(float deltaTime)
        {
            HandleKeyboard();
        }

        private void HandleKeyboard()
        {
            if (Input.GetKey(KeyCode.A))
            {
                MoveLeft();
            }
            else if (Input.GetKey(KeyCode.D))
            {
                MoveRight();
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                Fire();
            }
        }

        private void Fire()
        {
            OnFire?.Invoke();
        }

        private void MoveRight()
        {
            OnMoveRight?.Invoke();
        }
    
        private void MoveLeft()
        {
            OnMoveLeft?.Invoke();
        }
    }
}