﻿using System.Collections.Generic;
using UnityEngine;

namespace Homeworks.Other.ServicesLocator
{
    public class ServiceLocatorInstaller : MonoBehaviour
    {
        [SerializeField] private List<MonoBehaviour> services = new List<MonoBehaviour>();

        private void Awake()
        {
            foreach (var service in services)
            {
                ServiceLocator.AddService(service);
            }
        }
    }
}
