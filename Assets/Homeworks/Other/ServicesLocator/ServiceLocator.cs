using System;
using System.Collections.Generic;

namespace Homeworks.Other.ServicesLocator
{
    public static class ServiceLocator
    {
        private static readonly List<object> services = new List<object>();

        public static T GetService<T>()
        {
            foreach (T service in services)
            {
                if (service is T result)
                {
                    return result;
                }
            }

            throw new Exception($"Нет сервиса {typeof(T).Name}");
        }

        public static object GetService(Type serviceType)
        {
            foreach (var service in services)
            {
                if (serviceType.IsInstanceOfType(service))
                {
                    return service;
                }
            }
        
            throw new Exception($"Нет сервиса {serviceType.Name}");
        }

        public static void AddService(object service)
        {
            services.Add(service);
        }
    }
}

