﻿using System.Collections.Generic;
using Homeworks.Other.DI;
using UnityEngine;
using Homeworks.Other.ServicesLocator;

namespace Homeworks.Other.Architecture
{
    public sealed class GameInstaller : MonoBehaviour
    {
        [SerializeField]
        private List<MonoBehaviour> services = new List<MonoBehaviour>();

        private void Awake()
        {
            InstallServices();
            InstallListeners();
            //ResolveDependencies(transform);
        }

        private void InstallServices()
        {
            foreach (var service in services)
            {
                ServiceLocator.AddService(service);
            }
        }

        private void InstallListeners()
        {
            //var gameManager = GetComponent<GameManager>();
            //var listeners = GetComponentsInChildren<IGameListener>();

            // foreach (var listener in listeners)
            // {
            //     //gameManager.AddListener(listener);
            // }
        }

        private void ResolveDependencies(Transform node)
        {
            var behaviours = node.GetComponents<MonoBehaviour>();
            foreach (var behaviour in behaviours)
            {
                DependencyInjector.Inject(behaviour);
            }

            foreach (Transform child in node)
            {
                ResolveDependencies(child);
            }
        }
    }
}