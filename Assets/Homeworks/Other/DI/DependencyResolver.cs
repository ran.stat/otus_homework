﻿using System;
using UnityEngine;

namespace Homeworks.Other.DI
{
    public sealed class DependencyResolver : MonoBehaviour
    {
        private void Start()
        {
            this.ResolveDependencies(this.transform);
        }

        private void ResolveDependencies(Transform node)
        {
            var behaviours = node.GetComponents<MonoBehaviour>();
            foreach (var behaviour in behaviours)
            {
                DependencyInjector.Inject(behaviour);
            }

            foreach (Transform child in node)
            {
                this.ResolveDependencies(child);
            }
        }
    }
}