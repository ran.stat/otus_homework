﻿using System;
using JetBrains.Annotations;

namespace Homeworks.Other.DI
{

    [MeansImplicitUse]
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class InjectAttribute : Attribute
    {
        
    }
}